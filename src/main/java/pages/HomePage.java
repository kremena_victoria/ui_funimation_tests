package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HomePage {

    private WebDriver driver;
    private By registrationLink = By.linkText("Register");
    private By loginLink = By.partialLinkText("Login");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public RegistrationPage clickRegistrationLink(){
        driver.findElement(registrationLink).click();
        return new RegistrationPage(driver);
    }

    public LoginPage clickLoginLink(){

        WebElement loginClickableLink = driver.findElement(loginLink);
        //Actions actions = new Actions(driver);
        //actions.moveToElement(loginClickableLink).click().perform();
        if (loginClickableLink.isDisplayed()) {
            loginClickableLink.click();
        }
        return new LoginPage(driver);
    }
}
