package loginUI;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.NewsfeedPage;

public class LoginTests extends BaseTests {

    @Test
    public void testSuccessfulLogin() throws InterruptedException {
        //driver.manage().timeouts().implicitlyWait(47, TimeUnit.SECONDS);
        LoginPage loginPage = homePage.clickLoginLink();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(1000);
        loginPage.setUsername("kremena_victoria@yahoo.com");
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        loginPage.setPassword("qwerty123");
        NewsfeedPage newsfeedPage = loginPage.clickLoginButton();
        Assert.assertEquals(newsfeedPage.getAlertText(),"✦ News ✦", "Incorrect indirection");
    }

}
