package createPostUI;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CreatePostPage;
import pages.LoginPage;
import pages.NewsfeedPage;
import pages.UserPostsListPage;

import java.util.concurrent.TimeUnit;

public class CreatePostTests extends BaseTests {

    @Test
    public void testSuccessfulCreatedPost() throws InterruptedException {
        //driver.manage().timeouts().implicitlyWait(47, TimeUnit.SECONDS);
        LoginPage loginPage = homePage.clickLoginLink();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(1000);
        loginPage.setUsername("kremena_victoria@yahoo.com");
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        loginPage.setPassword("qwerty123");
        NewsfeedPage newsfeedPage = loginPage.clickLoginButton();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(1000);

        CreatePostPage createPostPage = newsfeedPage.clickCreatePostButton();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(1000);

        createPostPage.setPostTitle("Selenium n-th post");
        createPostPage.createPostContent("Next Selenium generated post");
        createPostPage.selectPrivacy("private");
        UserPostsListPage userPostListPage = createPostPage.submitPost();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Thread.sleep(1000);
        Assert.assertEquals(userPostListPage.getHeaderPostListText(), "Post's list", "Wrong indirection.");



    }
}
