package registrationUI;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.RegistrationPage;
import pages.SuccessfulRegistrationPage;

import java.util.concurrent.TimeUnit;

public class RegistrationTests extends BaseTests {

    @Test
    public void testSuccessfulRegistration() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(47, TimeUnit.SECONDS);
        RegistrationPage registrationPage = homePage.clickRegistrationLink();
        Thread.sleep(1000);
        registrationPage.setFirstName("Kremena");
        registrationPage.setLastName("Stoyanova");
        registrationPage.setEmailAddress("kremena11@mailinator.com");
        registrationPage.confirmEmailAddress("kremena11@mailinator.com");
        registrationPage.setPassword("qwerty123");
        registrationPage.confirmPassword("qwerty123");
        registrationPage.selectAgreeTermsCheckbox();
        Thread.sleep(2000);
        SuccessfulRegistrationPage successfulRegPage = registrationPage.submitRegistrationForm();
        Thread.sleep(2000);
        Assert.assertTrue(successfulRegPage.getSuccessfulRegistrationText().contains("You have successfully registered"),
                "Not successful registration.");
    }

}
