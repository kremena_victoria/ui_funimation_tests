package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;
    private By usernameField = By.id("username");
    private By passwordField = By.id("password");
    private By loginButton = By.id("login-submit");

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public void setUsername(String emailAddress){
        driver.findElement(usernameField).sendKeys(emailAddress);
    }

    public void setPassword(String password) {
        driver.findElement(passwordField).sendKeys(password);
    }

    public NewsfeedPage clickLoginButton(){
        driver.findElement(loginButton).click();
        return new NewsfeedPage(driver);
    }
}
