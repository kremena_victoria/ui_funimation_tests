package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NewsfeedPage {

    private WebDriver driver;
    private By statusAlert = By.xpath("//span[contains(text(),'News')]");
    private By postButton = By.xpath("//*[@id=\"index-middle-column-1\"]/div[2]/div/div/div/a[1]/button");
    private By requestButton = By.xpath("//*[@id=\"index-middle-column-1\"]/div[2]/div/div/div/a[2]/button");
    //private By dropdownLinks = By.

    public NewsfeedPage(WebDriver driver){
        this.driver = driver;
    }

    public String getAlertText(){

        return driver.findElement(statusAlert).getText();
    }

    public CreatePostPage clickCreatePostButton(){
        driver.findElement(postButton).click();
        return new CreatePostPage(driver);
    }

    public CreateRequestForOfferPage clickRequestButton(){
        driver.findElement(requestButton).click();
        return new CreateRequestForOfferPage(driver);
    }
}
