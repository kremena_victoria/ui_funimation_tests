package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CreatePostPage {
    private WebDriver driver;
    private By postTitleField = By.id("postTitle");
    private By postContent = By.id("postMessage");
    private By privacyDropDown = By.id("privacy.privacyId");
    private By submitPostButton = By.xpath("//button[@class='w3-button w3-theme-d2 w3-margin']");

    public CreatePostPage(WebDriver driver){
        this.driver = driver;
    }

    public void setPostTitle(String postTitle){
        driver.findElement(postTitleField).sendKeys(postTitle);
    }

    public void createPostContent(String content){
        driver.findElement(postContent).sendKeys(content);
    }

    public void selectPrivacy(String option){
        Select dropdownPrivacy = new Select(driver.findElement(privacyDropDown));
        dropdownPrivacy.selectByVisibleText(option);
    }

    public UserPostsListPage submitPost(){
        driver.findElement(submitPostButton).click();
        return new UserPostsListPage(driver);
    }
}
