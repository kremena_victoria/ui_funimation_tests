package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UserPostsListPage {

    private WebDriver driver;
    private By postListHeader = By.xpath("//h2[@class='header']");

    public UserPostsListPage(WebDriver driver){
        this.driver = driver;
    }

    public String getHeaderPostListText(){
        return driver.findElement(postListHeader).getText();
    }
}
