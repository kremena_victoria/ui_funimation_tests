package createRequestForOfferUI;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CreateRequestForOfferPage;
import pages.LoginPage;
import pages.NewsfeedPage;
import pages.UserPostsListPage;


public class CreateRequestBecomeAnimator extends BaseTests {

        @Test
        public void testSuccessfulRequestBecomeAnimator() throws InterruptedException{
            //driver.manage().timeouts().implicitlyWait(47, TimeUnit.SECONDS);
            LoginPage loginPage = homePage.clickLoginLink();
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(1000);
            loginPage.setUsername("kremena_victoria@yahoo.com");
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            loginPage.setPassword("qwerty123");
            NewsfeedPage newsfeedPage = loginPage.clickLoginButton();
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(1000);

            CreateRequestForOfferPage createRequestForofferPage = newsfeedPage.clickRequestButton();
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(3000);
            createRequestForofferPage.chooseRequestForBecomeComedian();
            createRequestForofferPage.setPublicAge("11");
            createRequestForofferPage.setLocation("Sofia");
            createRequestForofferPage.setDataInCalendar("12312020");
            createRequestForofferPage.setTimeOfParty("12.00");
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            UserPostsListPage userPostListPage = createRequestForofferPage.submitRequest();
            Thread.sleep(1000);

            Assert.assertEquals(userPostListPage.getHeaderPostListText(), "Post's list", "Wrong indirection.");
        }
    }
