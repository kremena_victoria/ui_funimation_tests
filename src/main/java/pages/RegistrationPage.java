package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class RegistrationPage {
    private WebDriver driver;
    private By firstNameField = By.id("firstName");
    private By lastNameField = By.id("lastName");
    private By emailField = By.id("email");
    private By confirmEmailField = By.id("confirmEmail");
    private By passwordField = By.id("password");
    private By confirmPasswordField = By.id("confirmPassword");
    private By controlLabel = By.className("control-label");
    private By termsAndConditions = By.partialLinkText("terms and conditions");
    private By agreeTermsCheckbox = By.xpath("//input[@id='terms']");
    private By registerButton = By.cssSelector(".form-group > button");

    public RegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    public void setFirstName(String firstName){
        driver.findElement(firstNameField).sendKeys(firstName);
    }

    public void setLastName(String lastName){
        driver.findElement(lastNameField).sendKeys(lastName);
    }

    public void setEmailAddress(String emailAddress){
        driver.findElement(emailField).sendKeys(emailAddress);
    }

    public void confirmEmailAddress(String emailAddress){
        driver.findElement(confirmEmailField).sendKeys(emailAddress);
    }

    public void setPassword(String pass){
        driver.findElement(passwordField).sendKeys(pass);
    }

    public void confirmPassword(String pass){
        driver.findElement(confirmPasswordField).sendKeys(pass);
    }

    public void readTermsAndConditions(){
        driver.findElement(termsAndConditions).click();
    }

    public void selectAgreeTermsCheckbox(){
        if ( !driver.findElement(agreeTermsCheckbox).isSelected())
        {
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(agreeTermsCheckbox)).click().perform();
        }
    }

    public SuccessfulRegistrationPage submitRegistrationForm(){
        driver.findElement(registerButton).click();
        return new SuccessfulRegistrationPage(driver);
    }
}
