package base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class BaseTests {

    protected WebDriver driver;
    protected HomePage homePage;

    @BeforeClass
    public void setUp() throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "webDrivers/chromedriver.exe");
        driver = new ChromeDriver();

        driver.get("http://funimation1.herokuapp.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("html/body/div/div/input")).click();

        Thread.sleep(5000);
        homePage = new HomePage(driver);
        //homePage.openCurtain();
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }
}

