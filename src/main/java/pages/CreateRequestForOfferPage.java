package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateRequestForOfferPage {

    private WebDriver driver;
    private By radioBtnKidsParty = By.xpath("//*[@id=\"title1\"]");
    private By radioBtnForComedian = By.xpath("//*[@id=\"title2\"]");
    private By publicAgeField = By.xpath("//*[@id=\"age\"]");
    private By cityLocation = By.xpath("//*[@id=\"location\"]");
    private By calendarField = By.xpath("//*[@id=\"date\"]");
    private By timeOfPartyField = By.xpath("//*[@id=\"duration\"]");
    private By requestSubmitButton = By.xpath("//*[@id=\"background-page\"]/div[3]/div/div/div/div/form/button");

    public CreateRequestForOfferPage(WebDriver driver){
        this.driver = driver;
    }

    public void chooseRequestForKidsParty(){
        driver.findElement(radioBtnKidsParty).click();
    }

    public void chooseRequestForBecomeComedian(){
        driver.findElement(radioBtnForComedian).click();
    }

    public void setPublicAge(String age){
        driver.findElement(publicAgeField).sendKeys(age);
    }

    public void setLocation(String city){
        driver.findElement(cityLocation).sendKeys(city);
    }

    public void setDataInCalendar(String data){
        driver.findElement(calendarField).sendKeys(data);
    }

    public void setTimeOfParty(String beginTime){
        driver.findElement(timeOfPartyField).sendKeys(beginTime);
    }

    public UserPostsListPage submitRequest(){
        driver.findElement(requestSubmitButton).click();
        return new UserPostsListPage(driver);
    }
}
