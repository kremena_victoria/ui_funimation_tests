package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SuccessfulRegistrationPage {
    private WebDriver driver;
    private By successfulRegistrationAlert = By.cssSelector(".alert.alert-info span");

    public SuccessfulRegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    public String getSuccessfulRegistrationText(){

        return driver.findElement(successfulRegistrationAlert).getText();
    }
}
